﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomCamera : MonoBehaviour
{
    public Transform roomToFollow;
    [SerializeField] Vector3 offset;
    [SerializeField] float damping;

    private void Start()
    {
        
    }

    private void LateUpdate()
    {
        Vector3 desiredPosition = roomToFollow.transform.position + offset;
        transform.position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * damping);
    }
}
