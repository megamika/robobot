﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraightBullet : MonoBehaviour, PlayerBullet
{
    public float knockback { get; set; }
    public float damage { get; set; }
    public string myTeam { get; set; }
    public float speed { get; set; }
    public float explosivness { get; set; }
    public float pierceThrough { get; set; }
    [SerializeField] new Rigidbody rigidbody;
    [SerializeField] GameObject explosion;

    [SerializeField] float mySpeed;
    [SerializeField] float myDamage;
    [SerializeField] float myKnockback;

    float health;

    void Start()
    {
        rigidbody.AddRelativeForce(Vector3.forward * speed * mySpeed, ForceMode.Impulse);
        health = pierceThrough;
    }

    void FixedUpdate()
    {
        rigidbody.AddRelativeForce(Vector3.forward * speed * mySpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        var hasHealth = other.gameObject.GetComponent<Abilities.Health.HasIt>();
        if (hasHealth != null)
        {
            if (hasHealth.health.parent.Team != myTeam)
            {
                hasHealth.health.Damage(damage * myDamage, knockback * myKnockback, transform.position);
                health -= 1f;

                if (health <= 0f) 
                    DestroySelf();
            }
        }

        if (other.tag == "Wall")
        {
            DestroySelf();
        }
    }

    void DestroySelf()
    {
        if (explosivness > 0f)
        {
            Explosion spawnedExplosion = Instantiate(explosion, transform.position, Quaternion.identity).GetComponent<Explosion>();
            spawnedExplosion.scale = explosivness;
        }
        Destroy(gameObject);
        
    }
}

public interface PlayerBullet
{
    float knockback { get; set; }
    float damage { get; set; }
    float speed { get; set; }
    float explosivness { get; set; }
    float pierceThrough { get; set; }
    string myTeam { get; set; }
}
