﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade : MonoBehaviour
{
    [SerializeField] UpgradeType upgradeType;
    Player player;

    private void Start()
    {
        player = FindObjectOfType<Player>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            switch (upgradeType)
            {
                case UpgradeType.Explosivness:
                    player.bulletExplosivnessUpgrade += 1;
                    break;
                case UpgradeType.Helpers:
                    break;
                case UpgradeType.Magnetism:
                    break;
                default:
                    break;
            }
            Destroy(gameObject);
        }
    }

    enum UpgradeType
    {
        Explosivness,
        Helpers,
        Magnetism,
    }
}
