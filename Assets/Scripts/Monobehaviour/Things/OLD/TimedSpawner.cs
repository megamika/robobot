﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedSpawner : MonoBehaviour, ZeroToOneFillup
{
    [SerializeField] float time;
    [SerializeField] GameObject spawned;

    public float fillup { get; private set; }

    private void Awake()
    {
    }

    private void Start()
    {
        Deploy();
    }

    public void Deploy()
    {
        gameObject.SetActive(true);
        StartCoroutine(Timer());
    }

    IEnumerator Timer()
    {
        float timer = 0f;
        while (timer <= time)
        {
            yield return null;
            timer += Time.deltaTime;
            fillup = timer / time;
        }

        Instantiate(spawned, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}

public interface ZeroToOneFillup
{
    float fillup { get; }
}
