﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MikaBehaviour
{
    public new Rigidbody rigidbody;
    public new Collider collider;
}
