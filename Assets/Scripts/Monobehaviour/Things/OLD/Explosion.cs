﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [SerializeField] float damage;
    [SerializeField] float radius;
    [SerializeField] float force;
    [SerializeField] float life;
    public float scale;

    private void Start()
    {
        Transform[] transforms = GetComponentsInChildren<Transform>();
        foreach (var transfor in transforms)
        {
            transfor.localScale *= scale;
        }
        radius *= scale;
        force *= scale;
        damage *= scale;

        Collider[] explodedThings = Physics.OverlapSphere(transform.position, radius);
        foreach (var thing in explodedThings)
        {
            if (thing.isTrigger)
                continue;

            Abilities.Health.HasIt hasHealth = thing.GetComponent<Abilities.Health.HasIt>();
            if (hasHealth != null)
            {
                hasHealth.health.Damage(damage, 0f, null);
            }

            Rigidbody hasRigidbody = thing.GetComponent<Rigidbody>();
            if (hasRigidbody)
            {
                hasRigidbody.AddExplosionForce(force, transform.position, radius);
            }
        }
        StartCoroutine(timedDestroy());
    }

    IEnumerator timedDestroy()
    {
        yield return new WaitForSeconds(life);
        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
