﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterialOverFillup : MonoBehaviour
{
    [SerializeField] Renderer render;
    ZeroToOneFillup zeroToOneFillup;

    void Start()
    {
        zeroToOneFillup = GetComponent<ZeroToOneFillup>();
    }


    void Update()
    {
        render.material.color = Color.Lerp(Color.white, Color.red, zeroToOneFillup.fillup);
    }
}
