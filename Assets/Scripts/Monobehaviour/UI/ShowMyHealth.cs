﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMyHealth : MonoBehaviour
{
    [SerializeField] Transform healthPosition;

    private void Start()
    {

        FindObjectOfType<HealthShowSystem>().AddHealthbar(GetComponent<Abilities.Health.HasIt>(), healthPosition);
    }
}
