﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Heathbar : MonoBehaviour
{
    [SerializeField] Image health;
    public float fillAmount
    {
        get
        {
            return health.fillAmount;
        }
        set
        {
            health.fillAmount = value;
        }
    }
}
