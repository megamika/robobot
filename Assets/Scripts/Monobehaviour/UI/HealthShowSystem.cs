﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthShowSystem : MonoBehaviour
{
    [SerializeField] GameObject healthbar;
    [SerializeField] Canvas canvas;
    [SerializeField] Camera mainCamera;
    List<HealthbarThing> healthbars = new List<HealthbarThing>();

    private void Update()
    {
        foreach (var healthbar in healthbars)
        {
            healthbar.heathbar.fillAmount = healthbar.health.healthPercent;
            healthbar.heathbar.transform.localPosition =
                CanvasPositioningExtensions.WorldToCanvasPosition(canvas, healthbar.position.transform.position, mainCamera);
        }
    }

    public void AddHealthbar(Abilities.Health.HasIt healthEntity, Transform healthPosition)
    {
        HealthbarThing newHealthbar = new HealthbarThing 
        { health = healthEntity.health, position = healthPosition, heathbar = createHealthbar() };
        healthbars.Add(newHealthbar);
        newHealthbar.health.OnDeathSimple += ()=> newHealthbar.heathbar.gameObject.SetActive(false);
    }

    Heathbar createHealthbar()
    {
        return Instantiate(healthbar, transform).GetComponent<Heathbar>();
    }

    class HealthbarThing
    {
        public Abilities.Health health;
        public Transform position;
        public Heathbar heathbar;
    }
}
