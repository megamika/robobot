﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotChose : MonoBehaviour
{
    [SerializeField] GameObject UI;

    private void Start()
    {
        UI.SetActive(false);
    }

    public ResultTimed ShowUI()
    {
        ResultTimed slotResult = new ResultTimed();
        StartCoroutine(SlotChoose());
        return slotResult;

        IEnumerator SlotChoose()
        {
            UI.SetActive(true);
            Result result;
            while (true)
            {
                if (Input.GetKeyDown(KeyCode.Q)) 
                { 
                    result = Result.Primary;
                    break;
                }
                if (Input.GetKeyDown(KeyCode.E)) 
                { 
                    result = Result.Secondary;
                    break;
                }
                if (Input.GetKeyDown(KeyCode.Escape)) 
                { 
                    result = Result.None;
                    break;
                }

                yield return null;
            }
            slotResult.result = result;
            slotResult.decizionIsFinalized = true;

            UI.SetActive(false);
        }
    }



    public class ResultTimed
    {
        public Result result;
        public bool decizionIsFinalized;
    }

    public enum Result
    {
        None,
        Primary,
        Secondary,
    }
}
