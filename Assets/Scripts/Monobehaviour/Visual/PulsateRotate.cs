﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulsateRotate : MonoBehaviour
{
    public AnimationCurve myCurve;
    [SerializeField] float height;
    [SerializeField] float speed;

    void Update()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, myCurve.Evaluate(((Time.time * speed) % 1f)) * height, transform.localPosition.z);
    }

}
