﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInput : MonoBehaviour
{
    static Default m_default;
    public static Default.PlayerActions Player => m_default.Player;

    private void Awake()
    {
        m_default = new Default();
    }

    private void OnEnable()
    {
        m_default.Enable();
    }

    private void OnDisable()
    {
        m_default.Disable();
    }

}
