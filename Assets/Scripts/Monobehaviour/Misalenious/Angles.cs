﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Angles
{
    public static float GetTehAngle(Vector3 infrom, Vector3 into)
    {
        Vector2 from = Vector2.up;
        Vector3 to = into - infrom;

        float ang = Vector2.Angle(from, to);
        Vector3 cross = Vector3.Cross(from, to);

        if (cross.z > 0)
            ang = 360 - ang;

        ang *= -1f;

        return ang;
    }

    public static float GetTehAngle(Vector3 into) => GetTehAngle(Vector3.zero, into);

    public static float Get3dTehAngle(Vector3 infrom, Vector3 into)
    {
        return GetTehAngle(new Vector3(infrom.x, infrom.z, 0), new Vector3(into.x, into.z, 0));
    }
}
