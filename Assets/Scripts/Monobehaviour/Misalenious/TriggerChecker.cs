﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class EntityTriggerChecker
{
    Trigger trigger;
    List<Entity> entitiesInside = new List<Entity>();

    void TriggerEnter(Collider other) =>Trigger(other, (Entity entity) => IfEntity(entity, true));
    void TriggerExit(Collider other) => Trigger(other, (Entity entity) => IfEntity(entity, false));
    

    void IfEntity(Entity entity, bool add)
    {
        if (EntityCheck(entity))
        {
            if (add)
                entitiesInside.Add(entity);
            else
                entitiesInside.Remove(entity);
        }
    }

    public abstract bool EntityCheck(Entity entity);

    void Trigger(Collider other, Action<Entity> ifIsEntity)
    {
        Entity isEntity = other.GetComponent<Entity>();

        if (isEntity) ifIsEntity(isEntity);
    }

    public EntityTriggerChecker(Trigger trigger)
    {
        this.trigger = trigger;
        trigger.enter += TriggerEnter;
        trigger.exit += TriggerExit;
    }
}
