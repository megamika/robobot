﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    Vector3 initialPosition;
    [SerializeField] Vector3 openOffset;
    [SerializeField] Transform spawn;
    public Vector3 spawnPosition => spawn.transform.position;
    public Enviroment parent { get; set; }
    public Trigger doorTrigger;

    public Direction direction;

    private void Awake()
    {
        initialPosition = transform.localPosition;
        doorTrigger.enter += triggerEnter;
    }

    public void Open()
    {
        transform.localPosition = initialPosition + openOffset;
        doorTrigger.enabled = true;
    }

    public void Close()
    {
        transform.localPosition = initialPosition;
    }

    void triggerEnter(Collider other)
    {
        if (other.tag == "Player")
            parent.DoorHasBeenOpened(direction);
    }
}
