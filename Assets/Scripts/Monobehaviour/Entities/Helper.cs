﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helper : FollowAndAttack
{
    Player daddy => GameManager.singleton.Player;

    public override void OnUpdate()
    {
        base.OnUpdate();
        if (targeting.closestEntity != null)
        {
            moving.SetDirection((targeting.closestEntity.transform.position - transform.position).normalized);
        }
        else
        {
            if (Vector3.Distance(transform.position, daddy.transform.position) > 3f)
            {

                moving.SetDirection((daddy.transform.position - transform.position).normalized);
            }
        }
    }
}
