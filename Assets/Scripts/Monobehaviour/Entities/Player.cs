﻿using Abilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Entity, Health.HasIt
{
    #region upgrades
    int _bulletSpeedUpgrade;
    public int bulletSpeedUpgrade
    {
        get => _bulletSpeedUpgrade;
        set
        {
            _bulletSpeedUpgrade = value;
            shooting.speed = 1 + 0.1f * value;
        }
    }

    int _bulletPierseUpgrade;
    public int bulletPierceUpgrade
    {
        get => _bulletPierseUpgrade;
        set
        {
            _bulletPierseUpgrade = value;
            shooting.pierceThrough = 1 + 0.25f;
        }
    }

    int _bulletExplosivnessUpgrade;
    public int bulletExplosivnessUpgrade
    {
        get => _bulletExplosivnessUpgrade;
        set
        {
            _bulletExplosivnessUpgrade = value;
            shooting.explosivness = 0.05f * value;
        }
    }

    int _numOfHelpersUpgrade;
    public int numOfHelpersUpgrade
    {
        get => _numOfHelpersUpgrade;
        set
        {
            _numOfHelpersUpgrade = value;
            helpers.SetHelperNumber(value);
        }
    }
    #endregion

    Health Health.HasIt.health => health;

    [SerializeField] Abilities.Movement movement;
    [SerializeField] Abilities.Knockback knockback;
    [SerializeField] Abilities.Health health;
    [SerializeField] Abilities.BulletShooting shooting;
    [SerializeField] Abilities.Helpers helpers;

    [SerializeField] Rotators.Rotation rotation;
    [SerializeField] Rotators.FollowTarget followMouse;
    [SerializeField] Rotators.LookTowardsMovement movementLook;
    bool IsShooting;
    [SerializeField] Transform testLookat;

    [SerializeField] float YPlane;

    Camera mainCam;
    Vector3 mousePosition;

    public override void OnAssignAbilities()
    {
        base.OnAssignAbilities();
        followMouse.target = testLookat;
        movementLook.targetMovement = movement;
        knockback.health = health;



        rotation.SetRotators(followMouse, movementLook);
        AddAbilities(movement, health, knockback, shooting, helpers, rotation);
    }

    public override void OnAssign()
    {
        base.OnAssign();
        
        _bulletSpeedUpgrade = 0;
        mainCam = Camera.main;
    }

    public override void OnStart()
    {
        base.OnStart();
        GameInput.Player.Move.performed += (ctx) => movement.SetDirection(directionToMovementDirection(ctx.ReadValue<Vector2>()));
        GameInput.Player.Move.canceled += (ctx) => movement.SetDirection(directionToMovementDirection(ctx.ReadValue<Vector2>()));
        GameInput.Player.MousePosition.performed += (ctx) => mousePosition = ctx.ReadValue<Vector2>();
        GameInput.Player.Shoot.performed += (_) => shooting.StartShooting();
        GameInput.Player.Shoot.performed += (_) => IsShooting = true;
        GameInput.Player.Shoot.canceled += (_) => shooting.StopShooting();
        GameInput.Player.Shoot.canceled += (_) => IsShooting = false;


        numOfHelpersUpgrade = 4;
        bulletExplosivnessUpgrade = 5;
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
        testLookat.transform.position = getMouseWorldPos();

        followMouse.disabled = !IsShooting;
        movementLook.disabled = IsShooting;
    }

    Vector3 getMouseWorldPos()
    {
        Vector3 result = Vector3.zero;
        Plane mousePlane = new Plane(Vector3.up, YPlane);
        Ray ray = mainCam.ScreenPointToRay(mousePosition);

        Debug.DrawRay(ray.origin, ray.direction * 40, Color.red);

        float enter;
        if (mousePlane.Raycast(ray, out enter))
        {
            result = ray.GetPoint(enter);
        }
        return result;
    }

    Vector3 directionToMovementDirection(Vector2 direction) => new Vector3(direction.x, 0, direction.y);
    

}
