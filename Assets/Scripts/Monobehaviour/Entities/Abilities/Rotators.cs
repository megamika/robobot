﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using static Entity;

namespace Rotators
{

    [System.Serializable]
    public class Rotation : Abilities.Ability
    {
        Rotator[] rotators;
        public void SetRotators(params Rotator[] rotators)
        {
            this.rotators = rotators;

        }

        public override void Initialized()
        {
            base.Initialized();

            if (rotators != null)
            {
                foreach (var rotator in rotators)
                {
                    rotator.parent = parent;
                    rotator.Initialized();
                }
            }
        }

        public override void Update()
        {
            base.Update();
            if (disabled) return;

            Quaternion finalRotation = parent.transform.rotation;
            int maxPriority = -1;
            if (rotators == null)
            {
                return;
            }
            foreach (var rotator in rotators)
            {
                if (!rotator.disabled) rotator.Update();

                if (!rotator.disabled & rotator.priority > maxPriority)
                {
                    finalRotation = rotator.rotation;
                    maxPriority = rotator.priority;
                }
            }
            parent.transform.rotation = finalRotation;
        }
    }

    public class Rotator
    {
        [SerializeField] int m_priority;
        public int priority => m_priority;
        public Entity parent { get; set; }
        public Quaternion rotation { get; set; }

        public bool disabled { get; set; }
        public Transform transform
        {
            get => parent.transform;
        }

        public virtual void Update() { }
        public virtual void Initialized() { }
    }

    [System.Serializable]
    public class LookTowardsMovement : Rotator
    {
        public Abilities.Movement targetMovement { get; set; }
        [SerializeField] float damping;

        public override void Initialized()
        {
            base.Initialized();
            Assert.IsNotNull(targetMovement, "target movement is Null");
        }

        public override void Update()
        {
            base.Update();
            if (targetMovement.desiredDirection.magnitude > 0f & !disabled)
            {
                rotation = Quaternion.Slerp(transform.rotation,
    Quaternion.LookRotation(targetMovement.desiredDirection, Vector2.up), Time.deltaTime * damping);
            }
        }
    }

    [System.Serializable]
    public class LookAtOnce : Rotator
    {
        public override void Initialized()
        {
            base.Initialized();
            disabled = false;
        }

        public void Perform(Vector3 target, float time)
        {
            IEnumerator performing()
            {
                rotation = Quaternion.Euler(parent.transform.eulerAngles.x,
                    Angles.GetTehAngle(parent.transform.position, target), parent.transform.eulerAngles.z);
                disabled = false;
                yield return new WaitForSeconds(time);
                disabled = true;
            }

            parent.StartCoroutine(performing());

        }
    }

    [System.Serializable]
    public class FollowTarget : Rotator
    {
        public Transform target { private get; set; }

        public override void Update()
        {
            base.Update();
            rotation = Quaternion.Euler(parent.transform.eulerAngles.x,
                    -Angles.Get3dTehAngle(parent.transform.position, target.transform.position), parent.transform.eulerAngles.z);
        }
    }
}
