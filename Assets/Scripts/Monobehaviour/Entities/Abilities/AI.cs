﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;
using static Entity;

namespace AI
{

    [System.Serializable]
    public class AI : Abilities.Ability
    {
        [SerializeField] NavMeshAgent agent;
        Actions.AIAction[] aiActions;
        ActionLoops.ActionLoop[] actionLoops;
        ActionLoops.ActionLoop activeLoop;

        public void SetActionLoops(params ActionLoops.ActionLoop[] actionLoops)
        {
            this.actionLoops = actionLoops;
        }

        public void SetAIAbilities(params Actions.AIAction[] actions)
        {
            aiActions = actions;
        }

        public void StartLoop(ActionLoops.ActionLoop actionLoop)
        {
            if (activeLoop != null)
                activeLoop.StopLoop();

            activeLoop = actionLoop;
            actionLoop.StartLoop();
        }

        public override void Initialized()
        {
            base.Initialized();

            agent.updatePosition = false;
            agent.updateRotation = false;

            foreach (var ability in aiActions)
            {
                ability.agent = agent;
                ability.parent = parent;
                ability.Initialized();
            }

            foreach (var actionLoop in actionLoops)
            {
                actionLoop.parent = parent;
            }
        }

        public override void Update()
        {
            base.Update();
            agent.velocity = parent.rigidbody.velocity;
        }

    }

    namespace Actions
    {

        public class AIAction
        {
            public NavMeshAgent agent { get; set; }
            public Entity parent { get; set; }

            public virtual void Initialized() { }
        }

        [Serializable]
        public class GotoPosition : AIAction
        {
            Abilities.Movement movement;
            [SerializeField] float reachDistance;

            public void SetValues(Abilities.Movement movement)
            {
                this.movement = movement;
            }

            public IEnumerator perform(Transform target, float? reachDistance, ActionResult actionResult)
            {
                actionResult.Reset();
                float reachDistanceIs = getReachDistance(reachDistance);

                while (true)
                {
                    agent.SetDestination(target.transform.position);
                    movement.SetDirection(agent.desiredVelocity);
                    if (Vector3.Distance(parent.transform.position, target.transform.position) < reachDistanceIs)
                    {
                        movement.SetDirection(Vector3.zero);
                        actionResult.isReady = true;
                        actionResult.result = ActionResult.Result.Success;
                        break;
                    }
                    yield return null;
                }
            }

            float getReachDistance(float? reachDistance)
            {
                float result;
                if (reachDistance.HasValue)
                    result = reachDistance.Value;
                else
                    result = this.reachDistance;
                return result;
            }
        }

        public class AttackTarget : AIAction
        {
            Abilities.ContiniousAttack continiousAttack;
            Rotators.LookAtOnce lookAt;
            Action attack;

            bool finishedAttack;

            public void SetValues(Abilities.ContiniousAttack continiousAttack, Action attack, Rotators.LookAtOnce lookAt)
            {
                this.lookAt = lookAt;
                this.continiousAttack = continiousAttack;
            }

            public override void Initialized()
            {
                base.Initialized();
                continiousAttack.onStopAttack += () => finishedAttack = true;
            }

            public IEnumerator perform(Entity target, ActionResult actionResult)
            {
                Abilities.Health.HasIt hasHealth = (target as Abilities.Health.HasIt);
                Abilities.Health targetHealth = null;
                if (hasHealth != null)
                    targetHealth = hasHealth.health;

                lookAt.Perform(target.transform.position, 0.1f);
                finishedAttack = false;
                attack?.Invoke();
                var damageRecorder = continiousAttack.AddDamageRecorder();
                yield return new WaitUntil(() => finishedAttack);
                actionResult.result = ActionResult.Result.Unknown;
                if (targetHealth != null)
                {
                    if (damageRecorder.healthDamaged.Contains(targetHealth))
                        actionResult.result = ActionResult.Result.Success;
                    else
                        actionResult.result = ActionResult.Result.Failure;
                }
                continiousAttack.RemoveDamageRecorder(damageRecorder);
            }
        }
    }

    public class ActionResult
    {
        public Result result;
        public bool isReady;

        public void Reset()
        {
            result = default;
            isReady = false;
        }

        public enum Result
        {
            Unknown,
            Success,
            Failure
        }
    }

    namespace ActionLoops
    {

        public abstract class ActionLoop
        {
            public Entity parent { get; set; }
            public abstract void StartLoop();
            public abstract void StopLoop();
        }

        [Serializable]
        public class GoToTargetAndAttack : ActionLoop
        {
            [SerializeField] Entity target;
            Actions.GotoPosition gotoPosition;
            Actions.AttackTarget attackTarget;
            Coroutine loopCoroutine;

            public void SetActions(Actions.GotoPosition gotoPosition, Actions.AttackTarget attackTarget)
            {
                this.gotoPosition = gotoPosition;
                this.attackTarget = attackTarget;
            }

            public override void StartLoop()
            {
                IEnumerator loop()
                {
                    while (true)
                    {
                        ActionResult actionResult = new ActionResult();
                        yield return gotoPosition.perform(target.transform, null, actionResult);
                        yield return attackTarget.perform(target, actionResult);
                    }
                }

                loopCoroutine = parent.StartCoroutine(loop());
            }

            public override void StopLoop()
            {
                parent.StopCoroutine(loopCoroutine);
            }
        }
    }

}