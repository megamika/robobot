﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using static Entity;

namespace Abilities
{
    public class Ability
    {
        public Entity parent { get; set; }
        public bool disabled { get; set; }

        public virtual void Initialized() { }
        public virtual void FixedUpdate() { }
        public virtual void Update() { }
    }

    [System.Serializable]
    public class Movement : Ability, Mover
    {
        Entity.Velocity velocity = new Velocity();
        public float speed;
        public Vector3 desiredDirection => velocity.value.normalized;
        Vector3 lastDirection;
        public float maxSpeed => speed;

        float moveMulti;
        public float movementMultiplyer
        {
            get => moveMulti;
            set
            {
                moveMulti = value;
                SetDirection(lastDirection);
            }
        }

        public override void Initialized()
        {
            base.Initialized();
            parent.velocities.Add(velocity);
            movementMultiplyer = 1f;
        }

        public override void Update()
        {
            base.Update();
            velocity.disabled = disabled;
        }

        public void SetDirection(Vector3 direction)
        {
            lastDirection = direction;
            direction = Vector3.ClampMagnitude(direction, 1f);
            velocity.value = direction * speed * movementMultiplyer;
        }
    }

    [System.Serializable]
    public class Helpers : Ability
    {
        [SerializeField] GameObject entityHelper;
        List<Entity> helpers = new List<Entity>();
        int numberOfHelpers => helpers.Count;

        public override void Initialized()
        {
            base.Initialized();
            GameManager.singleton.spawnPositionGotUpdated += () =>
            {
                foreach (var helper in helpers)
                {
                    helper.transform.position = GameManager.singleton.spawnPosition;
                }
            };
        }

        public void SetHelperNumber(int howMany)
        {
            int addEntities = howMany - numberOfHelpers;

            if (addEntities > 0)
            {
                for (int i = 0; i < addEntities; i++)
                {
                    AddHelper();
                }
            }
        }

        public void AddHelper()
        {
            Entity entity = Object.Instantiate(entityHelper).GetComponent<Entity>();
            helpers.Add(entity);
        }
    }

    [System.Serializable]
    public class SearchForTargets : Ability
    {
        [SerializeField] Trigger trigger;
        [SerializeField] float distanceCheckFrequency;
        List<Entity> entities = new List<Entity>();
        public Entity closestEntity { get; private set; }

        public override void Initialized()
        {
            base.Initialized();
            trigger.enter += triggerEnter;
            trigger.exit += triggerExit;
            parent.StartCoroutine(DistanceCheck());
        }

        void triggerEnter(Collider other)
        {
            Entity isEntity = other.GetComponent<Entity>();

            if (isEntity)
                if (isEntity.Team != parent.Team)
                    entities.Add(isEntity);
        }

        void triggerExit(Collider other)
        {
            Entity isEntity = other.GetComponent<Entity>();

            if (isEntity)
                if (isEntity.Team != parent.Team)
                    entities.Remove(isEntity);
        }

        IEnumerator DistanceCheck()
        {
            while (true)
            {
                List<Entity> entitiesToRemove = new List<Entity>();
                foreach (var entity in entities)
                {
                    if (!entity.gameObject.activeSelf)
                    {
                        entitiesToRemove.Add(entity);
                    }
                }
                foreach (var entity in entitiesToRemove)
                {
                    entities.Remove(entity);
                }

                Entity closeEntity = null;
                float minDistance = float.MaxValue;
                foreach (var entity in entities)
                {
                    float distance = Vector3.Distance(entity.transform.position, parent.transform.position);
                    if (distance < minDistance)
                    {
                        closeEntity = entity;
                        minDistance = distance;
                    }
                }
                closestEntity = closeEntity;


                yield return new WaitForSeconds(distanceCheckFrequency);
            }
        }
    }


    [System.Serializable]
    public class RandomiseMovement : Ability
    {
        [SerializeField] float minSpeed;
        [SerializeField] float maxSpeed;
        [SerializeField] float minInterval;
        [SerializeField] float maxInterval;
        public Movement movement;

        public override void Initialized()
        {
            base.Initialized();
            Assert.IsNotNull(movement, "You forgot to assign movement here");

            parent.StartCoroutine(loop());
        }

        IEnumerator loop()
        {
            while (true)
            {
                movement.speed = Random.Range(minSpeed, maxSpeed);
                yield return new WaitForSeconds(Random.Range(minInterval, maxInterval));
            }
        }
    }

    [System.Serializable]
    public class BulletShooting : Ability
    {
        [SerializeField] Transform source;
        [SerializeField] GameObject bullet;
        [SerializeField] bool isAutomatic;
        [SerializeField] float cooldownTime;
        [Space(20)]
        [SerializeField] float damage;
        [SerializeField] float knockback;
        public float explosivness;
        public float pierceThrough;
        public float speed;

        float cooldown;
        bool isShooting;
        bool isInCooldown;

        public void StartShooting()
        {
            if (isAutomatic)
                isShooting = true;
            if (cooldown <= 0f)
            {
                IEnumerator SpawnBulletLate()
                {
                    yield return null;
                    SpawnBullet();
                }
                parent.StartCoroutine(SpawnBulletLate());
                cooldown = cooldownTime;
            }
        }

        public void StopShooting()
        {
            isShooting = false;
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();

        }

        void SpawnBullet()
        {
            cooldown = cooldownTime;
            PlayerBullet spawnedBullet = 
                GameObject.Instantiate(bullet, source.transform.position, source.transform.rotation).GetComponent<PlayerBullet>();
            spawnedBullet.damage = damage;
            spawnedBullet.knockback = knockback;
            spawnedBullet.pierceThrough = pierceThrough;
            spawnedBullet.speed = speed;
            spawnedBullet.explosivness = explosivness;
            spawnedBullet.myTeam = parent.Team;
        }

        public override void Update()
        {
            base.Update();
            cooldown -= Time.deltaTime;
            if (cooldown <= 0f)
            {
                cooldown = 0f;
            }

            if (cooldown <= 0 & isShooting & isAutomatic)
            {
                SpawnBullet();
            }

        }

    }

    [System.Serializable]
    public class GroundCheck : Ability
    {
        public bool isGrounded => groundChecker.isGrounded;
        [SerializeField] GroundChecker groundChecker;
    }

    [System.Serializable]
    public class Jumping : Ability
    {
        public GroundCheck groundCheck { get; set; }
        public float force;

        public override void Initialized()
        {
            base.Initialized();
            Assert.IsNotNull(groundCheck, "Ground check in jumping is null");
        }

        public void Perform()
        {
            if (groundCheck.isGrounded)
            {
                parent.rigidbody.AddForce(Vector3.up * force, ForceMode.VelocityChange);
            }
        }
    }

    [System.Serializable]
    public class Health : Ability
    {
        [SerializeField] float m_maxHealth;
        public float maxHealth => m_maxHealth;
        public float health { get; private set; }
        public float healthPercent => health / maxHealth;
        public bool isDead => health <= 0f;
        public System.Action<float, float, Vector3?> OnDamaged;
        public System.Action<float, float, Vector3?> OnDeath;
        public System.Action OnDeathSimple;
        [Space(20)]
        [SerializeField] bool DebugLogs;

        public override void Initialized()
        {
            base.Initialized();
            health = maxHealth;
        }

        public void Damage(float value, float knockback, Vector3? source)
        {
            if (health <= 0)
            {
                return;
            }
            health -= value;
            OnDamaged?.Invoke(value, knockback, source);
            if (health <= 0)
            {
                health = 0;
                OnDeath?.Invoke(value, knockback, source);
                OnDeathSimple?.Invoke();
                if (DebugLogs)
                {
                    Debug.Log("An entity " + parent.gameName + " has died.");
                }
            }

            if (DebugLogs)
            {
                Debug.Log("An entity " + parent.gameName + " got damaged. Value: " + value.ToString());
            }
        }

        public interface HasIt
        {
            Health health { get; }
        }

    }

    [System.Serializable]
    public class RigidbodifyOnDeath : Ability
    {
        [SerializeField] Rigidbodify rigidbodify;
        public Health health { get; set; }
        Ability[] abilitiesToDisable;

        public override void Initialized()
        {
            base.Initialized();
            health.OnDeath += OnDeath;
        }

        public void SetAbilitiesToDisable(params Ability[] abilities) => abilitiesToDisable = abilities;


        void OnDeath(float value, float knockback, Vector3? source)
        {
            if (abilitiesToDisable != null)
            {
                foreach (var ability in abilitiesToDisable)
                {
                    ability.disabled = true;
                }
            }
            parent.anim.enabled = false;
            parent.collider.enabled = false;
            parent.rigidbody.isKinematic = true;
            rigidbodify.Enable();
            rigidbodify.AddForce(knockback, source);

            parent.StartCoroutine(tempEnable());

            IEnumerator tempEnable()
            {
                yield return new WaitForSeconds(3f);
                rigidbodify.Disable();
                parent.model.gameObject.SetActive(false);
            }
        }
    }

    [System.Serializable]
    public class Knockback : Ability
    {
        public Health health { get; set; }
        [SerializeField] float knockbackMultiplier;

        public override void Initialized()
        {
            base.Initialized();
            Assert.IsNotNull(health, "You forgot to assign health to a knockback");

            health.OnDamaged += OnDamaged;
        }

        void OnDamaged(float value, float knockback, Vector3? source)
        {
            if (!source.HasValue)
                return;

            parent.rigidbody.AddForce((parent.transform.position - source.Value).normalized * knockbackMultiplier * knockback, ForceMode.VelocityChange);
        }
    }

    [System.Serializable]
    public class Attack : Ability
    {
        public Trigger trigger;
        public List<Health.HasIt> inRange = new List<Health.HasIt>();
        [Space(20)]
        [SerializeField] bool m_debugLogs;
        public bool debugLogs => m_debugLogs;

        public override void Initialized()
        {
            base.Initialized();
            trigger.enter += triggerEnter;
            trigger.exit += triggerExit;
        }

        public virtual void triggerEnter(Collider other)
        {
            Health.HasIt health = other.GetComponent<Health.HasIt>();
            if (health != null)
            {
                inRange.Add(health);
                healthTriggerEnter(health);
            }
        }

        public virtual void healthTriggerEnter(Health.HasIt health) { }

        public virtual void triggerExit(Collider other)
        {
            Health.HasIt health = other.GetComponent<Health.HasIt>();
            if (health != null)
            {
                inRange.Remove(health);
            }
        }
    }

    [System.Serializable]
    public class DoThingPeriodically : Ability
    {
        [SerializeField] float minWait;
        [SerializeField] float maxWait;
        public System.Action thing;

        public override void Initialized()
        {
            base.Initialized();
            parent.StartCoroutine(doingThing());
        }

        IEnumerator doingThing()
        {
            while (true)
            {
                thing?.Invoke();
                yield return new WaitForSeconds(Random.Range(minWait, maxWait));
            }
        }
    }

    [System.Serializable]
    public class QuickAttack : Attack
    {

        [SerializeField] float damage;
        [SerializeField] float knockback;
        public System.Action OnAttack;

        public void Attack()
        {
            OnAttack?.Invoke();
            foreach (var item in inRange)
            {
                item.health.Damage(damage, knockback, parent.transform.position);
            }

            if (debugLogs)
            {
                string message = "An entity " + parent.gameName + " has dealt " + damage.ToString() + " damage to the following entities:\n";
                foreach (var item in inRange)
                {
                    message += item.health.parent.gameName + ", ";
                }
                Debug.Log(message);
            }
        }

        public void AttackIfHasTargets()
        {
            if (inRange.Count == 0)
                return;

            Attack();
        }
    }



    public class Targeting : Ability
    {
        List<Entity> entitiesInZone = new List<Entity>();
        [SerializeField] Trigger trigger;
    }

    [System.Serializable]
    public class ContiniousAttack : Attack
    {
        List<Health.HasIt> gotDamaged = new List<Health.HasIt>();
        public MultiplyerValues speedMultiplyer;
        MultiplyerValues.Value myValue;
        public Rotators.Rotation rotation;
        [SerializeField] float dashWhenAttacking;
        [SerializeField] float moveSpeedWhenAttacking;
        [SerializeField] float damage;
        [SerializeField] float knockback;
        public AttackEventCatcher attackEventCatcher;
        [SerializeField] Renderer weaponRenderer;
        bool isAttacking;

        List<DamageRecorder> damageRecorders = new List<DamageRecorder>();
        public System.Action onStartAttack;
        public System.Action onStopAttack;

        public override void Initialized()
        {
            base.Initialized();

            if (speedMultiplyer != null)
                myValue = speedMultiplyer.AddMultiplyer(1f);

            attackEventCatcher.start += StartAttacking;
            attackEventCatcher.stop += StopAttacking;
            if (weaponRenderer != null)
                weaponRenderer.forceRenderingOff = true;
        }

        public void StartAttacking()
        {
            SwitchMoversAndRotators(true);
            parent.rigidbody.AddForce(parent.model.transform.forward * dashWhenAttacking, ForceMode.Impulse);

            if (weaponRenderer != null)
                weaponRenderer.forceRenderingOff = false;
            foreach (var health in inRange)
            {
                DealDamage(health);
            }
            isAttacking = true;

            if (debugLogs)
            {
                Debug.Log("Entity " + parent.gameName + " started attacking");
            }

            onStartAttack?.Invoke();
        }

        public void StopAttacking()
        {
            SwitchMoversAndRotators(false);

            if (weaponRenderer != null)
                weaponRenderer.forceRenderingOff = true;
            gotDamaged.Clear();
            isAttacking = false;

            if (debugLogs)
            {
                Debug.Log("Entity " + parent.gameName + " stopped attacking");
            }

            onStartAttack?.Invoke();
        }

        void SwitchMoversAndRotators(bool value)
        {
            if (myValue != null)
            {
                myValue.value = value ? moveSpeedWhenAttacking : 1f;
                Debug.Log("Entity " + parent.gameName + " set speed multiplyer to " + myValue.value.ToString());
            }


            if (rotation != null)
            {
                rotation.disabled = value;
            }
        }

        public override void healthTriggerEnter(Health.HasIt health)
        {
            if (!isAttacking || gotDamaged.Contains(health))
                return;

            DealDamage(health);
        }

        void DealDamage(Health.HasIt health)
        {
            health.health.Damage(damage, knockback, parent.transform.position);
            gotDamaged.Add(health);

            foreach (var damageRecorder in damageRecorders)
                damageRecorder.healthDamaged.Add(health.health);
        }

        public DamageRecorder AddDamageRecorder()
        {
            DamageRecorder newDamageRecorder = new DamageRecorder();
            damageRecorders.Add(newDamageRecorder);
            return newDamageRecorder;
        }

        public void RemoveDamageRecorder(DamageRecorder damageRecorder)
        {
            damageRecorders.Remove(damageRecorder);
        }

        public class DamageRecorder
        {
            public List<Health> healthDamaged = new List<Health>();
        }
    }

    [System.Serializable]
    public class Dash : Ability
    {
        [SerializeField] float force;
        [SerializeField] float cooldown;
        bool onCooldown;

        public void Perform(Vector3 direction)
        {
            if (onCooldown)
                return;

            direction.Normalize();
            direction = new Vector3(direction.x, 0, direction.z);
            if (direction.magnitude == 0f)
                return;

            parent.rigidbody.AddForce(direction * force, ForceMode.VelocityChange);
            parent.StartCoroutine(CooldownIE());
        }

        IEnumerator CooldownIE()
        {
            onCooldown = true;
            yield return new WaitForSeconds(cooldown);
            onCooldown = false;
        }
    }

    [System.Serializable]
    public class IteamPickup : Ability
    {
        List<EquipmentIteam> equipmentIteams = new List<EquipmentIteam>();
        [SerializeField] Trigger trigger;

        public override void Initialized()
        {
            base.Initialized();
            trigger.enter += triggerEnter;
            trigger.exit += triggerExit;
        }

        void triggerEnter(Collider other)
        {
            EquipmentIteam equipmentIteam = other.GetComponent<EquipmentIteam>();
            if (equipmentIteam != null)
            {
                equipmentIteams.Add(equipmentIteam);
            }
        }

        void triggerExit(Collider other)
        {
            EquipmentIteam equipmentIteam = other.GetComponent<EquipmentIteam>();
            if (equipmentIteam != null)
            {
                equipmentIteams.Remove(equipmentIteam);
            }
        }

        public EquipmentIteam GetClosest()
        {
            EquipmentIteam result = null;
            float minDistance = float.MaxValue;

            foreach (var item in equipmentIteams)
            {
                if (item == null)
                    continue;

                float distance = Vector3.Distance(parent.transform.position, item.transform.position);
                if (distance < minDistance)
                {
                    result = item;
                    minDistance = distance;
                }
            }
            return result;
        }

        public EquipmentIteam GetClosestToEquip()
        {
            EquipmentIteam result = GetClosest();
            if (result != null)
                equipmentIteams.Remove(result);
            return result;
        }

        public void IteamGotEquipped(EquipmentIteam iteam)
        {
            equipmentIteams.Remove(iteam);
        }
    }

    [System.Serializable]
    public class Enviromental : Ability
    {
        public Enviroment enviroment { get; private set; }

        public override void Initialized()
        {
            base.Initialized();
            enviroment = parent.GetComponentInParent<Enviroment>();
        }


    }
}