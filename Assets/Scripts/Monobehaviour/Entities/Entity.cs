﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.AI;
using System;
public class Entity : MikaBehaviour
{
    [SerializeField] string m_gameName = "Entity";
    [SerializeField] string m_Team = "Entity";
    public string Team => m_Team;
    public string gameName => m_Team;
    public List<Velocity> velocities = new List<Velocity>();
    public List<Abilities.Ability> abilities = new List<Abilities.Ability>();
    public new Rigidbody rigidbody;
    public new Collider collider;
    public Animator anim;
    public Transform model;

    public virtual void OnAssignVelocities() { }
    public virtual void OnAssignAbilities() { }

    public virtual void OnAutoAssign()
    {
        if (rigidbody == null)
            rigidbody = GetComponentInChildren<Rigidbody>();
        if (model == null)
            model = transform.GetChild(0);
        if (anim == null)
            anim = GetComponentInChildren<Animator>();
    }

    public override void OnAssign()
    {
        base.OnAssign();
        OnAutoAssign();
        OnAssignVelocities();
        OnAssignAbilities();
    }

    public void AddAbility(Abilities.Ability ability)
    {
        abilities.Add(ability);
            ability.parent = this;
        ability.Initialized();
    }

    public void AddAbilities(params Abilities.Ability[] abilities)
    {
        foreach (var ability in abilities)
        {
            AddAbility(ability);
        }
    }

    public override void OnFixedUpdate()
    {
        base.OnFixedUpdate();
        Vector3 allForces = Vector3.zero;
        foreach (var velocity in velocities)
        {
            allForces += velocity.value * System.Convert.ToInt32(!velocity.disabled);
        }

        rigidbody.AddForce(allForces, ForceMode.Acceleration);

        foreach (var ability in abilities)
        {
            ability.FixedUpdate();
        }
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
        foreach (var ability in abilities)
        {
            ability.Update();
        }
    }



    public class MultiplyerValues
    {
        List<Value> multiplyerValues = new List<Value>();
        public float finalValue
        {
            get
            {
                float result = 1f;
                foreach (var item in multiplyerValues)
                {
                    result *= item.value;
                }
                return result;
            }
        }

        public Value AddMultiplyer(float value)
        {
            Value newValue = new Value { value = value, parent = this };
            multiplyerValues.Add(newValue);
            return newValue;
        }

        public void RemoveMultiplyer(Value multiplyer)
        {
            multiplyerValues.Remove(multiplyer);
        }

        public class Value
        {
            public MultiplyerValues parent;
            public float value;
        }
    }




    public abstract class Equipment
    {
        public Sprite icon;
        public Sprite Icon { get; private set; }
        public Entity parent { get; private set; }

        public void Equip(Entity parent)
        {
            this.parent = parent;
            OnEquipped();
        }
        public void Unequip()
        {
            OnUnEquipped();
        }

        public virtual void OnPress() { }
        public virtual void OnRelease() { }
        public virtual void OnEquipped() { }
        public virtual void OnUnEquipped() { }
    }

    public interface SpeedIsAffectable
    {
        MultiplyerValues speedMultiplyer { get; }
    }

    public interface Mover
    {
        float movementMultiplyer { get; set; }
    }

    public class Grab
    {
        [SerializeField] Transform grab;
    }

    public class Velocity
    {
        public Vector3 value;
        public bool disabled;
    }
}
