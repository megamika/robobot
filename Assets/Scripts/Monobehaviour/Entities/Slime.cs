﻿using Abilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : FollowAndAttack
{

    public override void OnUpdate()
    {
        base.OnUpdate();
        if (targeting.closestEntity != null)
        {
            moving.SetDirection(getDirectionTowardsTarget());
        }
        else
        {
            moving.SetDirection(Vector3.zero);
        }
    }

    Vector3 getDirectionTowardsTarget()
    {
        Vector3 result = Vector3.Normalize(targeting.closestEntity.transform.position - transform.position);
        result = new Vector3(result.x, 0f, result.z);
        return result;
    }
}
