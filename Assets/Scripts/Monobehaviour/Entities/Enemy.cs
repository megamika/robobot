﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Entity, Abilities.Health.HasIt
{
    [SerializeField] Abilities.Health health;
    [SerializeField] Abilities.RigidbodifyOnDeath rigidbodifyOnDeath;
    [SerializeField] Abilities.Knockback knockback;
    [SerializeField] Abilities.Movement movement;
    [SerializeField] Abilities.ContiniousAttack continiousAttack;

    Rotators.Rotation rotation = new Rotators.Rotation();
    [SerializeField] Rotators.LookTowardsMovement lookTowardsMovement;
    Rotators.LookAtOnce lookAt = new Rotators.LookAtOnce();

    [SerializeField] float reachDistance;
    [SerializeField] Transform testDestination;

    [SerializeField] AI.AI ai;
    [SerializeField] AI.ActionLoops.GoToTargetAndAttack goToTargetAndAttack;
    [SerializeField] AI.Actions.GotoPosition goToPosition;
    [SerializeField] AI.Actions.AttackTarget attackTarget;



    public override void OnAssign()
    {
        base.OnAssign();
    }

    public override void OnAssignAbilities()
    {
        base.OnAssignAbilities();
        lookTowardsMovement.targetMovement = movement;
        rotation.SetRotators(lookTowardsMovement, lookAt);

        rigidbodifyOnDeath.health = health;
        rigidbodifyOnDeath.SetAbilitiesToDisable(movement, rotation);

        knockback.health = health;
        AddAbilities(health, rigidbodifyOnDeath, knockback, movement, continiousAttack);

        goToPosition.SetValues(movement);
        attackTarget.SetValues(continiousAttack, () => anim.SetTrigger("Attack"), lookAt);
        goToTargetAndAttack.SetActions(goToPosition, attackTarget);
        ai.SetActionLoops(goToTargetAndAttack);
        ai.SetAIAbilities(goToPosition, attackTarget);
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        anim.SetBool("Running", movement.desiredDirection.magnitude > 0.1f);
    }

    Abilities.Health Abilities.Health.HasIt.health => health;

    public string teamName => "Enemy";
    public int priority => 0;
}
