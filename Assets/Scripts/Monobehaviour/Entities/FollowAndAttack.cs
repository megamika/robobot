﻿using Abilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowAndAttack : Entity, Abilities.Health.HasIt
{
    [SerializeField] Abilities.RandomiseMovement randomMovement;
    [SerializeField] Abilities.Movement movement;
    [SerializeField] Abilities.SearchForTargets targetSearch;
    [SerializeField] Abilities.Health health;
    [SerializeField] Abilities.Knockback knockback;
    [SerializeField] Abilities.DoThingPeriodically attackSometimes;
    [SerializeField] Abilities.QuickAttack attack;

    [SerializeField] Rotators.Rotation rotation;
    [SerializeField] Rotators.LookTowardsMovement lookTowardsMovement;

    public Abilities.SearchForTargets targeting => targetSearch;
    public Abilities.Movement moving => movement;

    Health Health.HasIt.health => health;

    //[SerializeField]

    public override void OnAssignAbilities()
    {
        base.OnAssignAbilities();
        randomMovement.movement = movement;
        lookTowardsMovement.targetMovement = movement;
        knockback.health = health;
        attackSometimes.thing += attack.AttackIfHasTargets;

        rotation.SetRotators(lookTowardsMovement);
        AddAbilities(movement, randomMovement, targetSearch, health, knockback, attackSometimes, attack, rotation);
    }
}
