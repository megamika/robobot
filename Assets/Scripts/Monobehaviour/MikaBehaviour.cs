﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MikaBehaviour : MonoBehaviour
{


    public virtual void OnAssign() { }
    public virtual void OnAwake() { }

    private void Awake()
    {
        OnAssign();
        OnAwake();
    }

    public virtual void OnStart() { }

    private void Start()
    {
        OnStart();
    }

    public virtual void OnUpdate() { }

    private void Update()
    {
        OnUpdate();
    }

    public virtual void OnFixedUpdate() { }

    private void FixedUpdate()
    {
        OnFixedUpdate();
    }

    public virtual void OnLateUpdate() { }

    private void LateUpdate()
    {
        OnLateUpdate();
    }

    public virtual void OnOnTriggerStay(Collider other) { }

    private void OnTriggerStay(Collider other)
    {
        OnOnTriggerStay(other);
    }

}
