﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Player Player => player;
    [SerializeField] Player player;
    [SerializeField] RoomCamera roomCamera;
    [SerializeField] GameObject blankRoom;
    [SerializeField] GameObject[] roomInsides;
    [SerializeField] Vector3 roomSize;

    [SerializeField] int seed;
    System.Random random;
    System.Random randomDoor;
    System.Random randomRoom;

    Vector2Int playerRoomPosition;

    Direction directionPlayerExitedFrom;
    Enviroment _activeRoom;
    public Vector3 spawnPosition { get; private set; }
    public System.Action spawnPositionGotUpdated;

    Enviroment activeRoom
    {
        get => _activeRoom;
        set
        {
            roomCamera.roomToFollow = value.transform;
            _activeRoom = value;
        }
    }

    public static GameManager singleton;


    private void Awake()
    {
        singleton = this;
    }

    private void Start()
    {
        int seedInt = seed;
        random = new System.Random(seedInt);
        randomDoor = new System.Random(seedInt);
        randomRoom = new System.Random(seedInt);
        directionPlayerExitedFrom = Direction.forward;

        var playerHealth = player as Abilities.Health.HasIt;
        playerHealth.health.OnDeathSimple += () => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        StartCoroutine(roomLoop());
    }

    IEnumerator roomLoop()
    {
        while (true)
        {
            Enviroment spawnedRoom = InstantiateRoom(playerRoomPosition);
            Direction direction = OpenOppositeDoorToTheExitDirection(directionPlayerExitedFrom, spawnedRoom);
            spawnPosition = spawnedRoom.GetDoor(direction).spawnPosition;
            player.transform.position = spawnPosition;
            spawnPositionGotUpdated?.Invoke();
            Instantiate(roomInsides[randomRoom.Next(roomInsides.Length - 1)], spawnedRoom.transform);
            spawnedRoom.EntitiesGotSpawned();
            spawnedRoom.CloseAllDoors();
            spawnedRoom.GetDoor(Direction.back).Close();
            activeRoom = spawnedRoom;
            yield return new WaitUntil(()=> spawnedRoom.allEnemiesAreDead);
            OpenRandomDoors(spawnedRoom, getOpposite(directionPlayerExitedFrom));
            yield return new WaitUntil(()=> spawnedRoom.playerHasChosenAnExit);
            spawnedRoom.CloseAllDoors();
            AdvancePlayerPosition(spawnedRoom.playerChosenExit);
            directionPlayerExitedFrom = spawnedRoom.playerChosenExit;
        }
    }

    Direction getOpposite(Direction direction)
    {
        switch (direction)
        {
            case Direction.forward:
                return Direction.back;
            case Direction.back:
                return Direction.forward;
            case Direction.left:
                return Direction.right;
            case Direction.right:
                return Direction.left;
            default:
                return default;
        }
    }

    Direction OpenOppositeDoorToTheExitDirection(Direction exitDirection, Enviroment room)
    {
        Direction direction = getOpposite(exitDirection);
        room.OpenDoor(direction);
        return direction;
    }

    void AdvancePlayerPosition(Direction direction)
    {
        switch (direction)
        {
            case Direction.forward:
                playerRoomPosition += new Vector2Int(0, 1);
                break;
            case Direction.back:
                playerRoomPosition += new Vector2Int(0, -1);
                break;
            case Direction.left:
                playerRoomPosition += new Vector2Int(-1, 0);
                break;
            case Direction.right:
                playerRoomPosition += new Vector2Int(1, 0);
                break;
            default:
                break;
        }
    }

    void OpenRandomDoors(Enviroment room, Direction exceptionDirection)
    {
        List<Direction> directions = new List<Direction>
        {
            Direction.left,
            Direction.right,
            Direction.forward,
        };
        if (directions.Contains(exceptionDirection))
            directions.Remove(exceptionDirection);

        room.OpenDoor(directions[randomDoor.Next(directions.Count)]);
        room.OpenDoor(directions[randomDoor.Next(directions.Count)]);
    }

    Enviroment InstantiateRoom(Vector2Int position) => Instantiate(blankRoom, 
            Vector3.Scale(new Vector3(position.x, 0, position.y), roomSize), Quaternion.identity).GetComponent<Enviroment>();
    

}
