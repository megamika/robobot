﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enviroment : MonoBehaviour
{
    Entity[] entities;

    [SerializeField] Door forwardDoor;
    [SerializeField] Door backDoor;
    [SerializeField] Door leftDoor;
    [SerializeField] Door rightDoor;

    Door[] allDoors;

    public bool allEnemiesAreDead { get; private set; }
    public bool playerHasChosenAnExit { get; private set; }
    public Direction playerChosenExit { get; private set; }


    private void Start()
    {
        forwardDoor.direction = Direction.forward;
        backDoor.direction = Direction.back;
        leftDoor.direction = Direction.left;
        rightDoor.direction = Direction.right;
        allDoors = new Door[]
        {
            forwardDoor,
            backDoor,
            leftDoor,
            rightDoor
        };
        foreach (var door in allDoors)
            door.parent = this;
        

        StartCoroutine(triggerTimer());
    }

    public void EntitiesGotSpawned()
    {
        entities = GetComponentsInChildren<Entity>();

        foreach (var entity in entities)
        {
            var hasHealth = entity as Abilities.Health.HasIt;
            if (hasHealth != null)
            {
                hasHealth.health.OnDeathSimple += () => CheckIfEveryoneIsDead();
            }
        }
    }

    public void CloseAllDoors()
    {
        IEnumerator closeWhenCan()
        {
            yield return new WaitUntil(() => allDoors != null);

            foreach (var door in allDoors)
                door.Close();
        }
        StartCoroutine(closeWhenCan());
        
    }

    public void DoorHasBeenOpened(Direction direction)
    {
        playerChosenExit = direction;
        playerHasChosenAnExit = true;
    }

    public void OpenDoor(Direction direction)
    {
        GetDoor(direction).Open();
    }

    public Door GetDoor(Direction direction)
    {
        switch (direction)
        {
            case Direction.forward:
                return forwardDoor;
            case Direction.back:
                return backDoor;
            case Direction.left:
                return leftDoor;
            case Direction.right:
                return rightDoor;
            default:
                return null;
        }
    }

    void CheckIfEveryoneIsDead()
    {
        bool allEnemiesAreDeadTemp = true;
        foreach (var entity in entities)
        {
            Abilities.Health.HasIt hasHealth = entity as Abilities.Health.HasIt;
            if (hasHealth != null)
            {
                allEnemiesAreDeadTemp = hasHealth.health.isDead;
                if (allEnemiesAreDeadTemp == false)
                    break;
            }
        }
        allEnemiesAreDead = allEnemiesAreDeadTemp;
    }

    IEnumerator triggerTimer()
    {
        foreach (var door in allDoors)
        {
            door.doorTrigger.enabled = false;
        }

        yield return new WaitUntil(() => allEnemiesAreDead);


    }

}

public enum Direction
{
    forward,
    back,
    left,
    right
}
