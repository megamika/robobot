﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MikaBehaviour
{
    [SerializeField] WorldSO worldSO;
    public WorldSettings worldSettings { get; private set; }
    public static World singleton;

    public override void OnAssign()
    {
        base.OnAssign();
        worldSettings = worldSO.worldSettings;
        singleton = this;
    }
}
