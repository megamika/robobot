﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildOf : MonoBehaviour
{
    [SerializeField] Transform parent;
    Vector3 offset;
    [SerializeField] bool offsetAtStart;

    private void Start()
    {
        if (offsetAtStart)
        {
            offset = transform.position - parent.transform.position;
        }
    }

    private void LateUpdate()
    {
        if (!parent)
            return;
        
        transform.position = parent.transform.position + parent.transform.rotation * offset;
        transform.rotation = parent.transform.rotation;
    }
}
