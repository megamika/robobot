﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentOf : MikaBehaviour
{
    Transform parent;

    public void SetParent(Transform parent)
    {
        this.parent = parent;
    }

    public override void OnLateUpdate()
    {
        base.OnLateUpdate();
        if (parent != null)
        {
            transform.position = parent.transform.position;
            transform.rotation = parent.transform.rotation;
        }
    }
}
