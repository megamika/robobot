﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChecker : MikaBehaviour
{
    new Collider collider;
    LayerMask groundLayer => World.singleton.worldSettings.groundLayer;
    bool isGroundedInternal;
    public bool isGrounded { get; private set; }

    public override void OnStart()
    {
        base.OnStart();
    }

    public override void OnAssign()
    {
        base.OnAssign();
        collider = GetComponent<Collider>();
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
        isGrounded = isGroundedInternal;
    }

    public override void OnFixedUpdate()
    {
        base.OnFixedUpdate();
        isGroundedInternal = false;
    }

    public override void OnOnTriggerStay(Collider other)
    {
        base.OnOnTriggerStay(other);
        if (groundLayer == (groundLayer | (1 << other.gameObject.layer)))
        {
            isGroundedInternal = true;
        }
    }
}
