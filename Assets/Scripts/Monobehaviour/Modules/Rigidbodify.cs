﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rigidbodify : MonoBehaviour
{
    [SerializeField] Collider[] colliders;
    [SerializeField] Rigidbody[] rigidbodies;

    private void Awake()
    {
        Disable();
    }

    public void Enable() => Switch(true);
    public void Disable() => Switch(false);

    public void AddForce(float force, Vector3? source)
    {
        if (source.HasValue)
        {
            foreach (var rigidbody in rigidbodies)
                rigidbody.AddForce((rigidbody.transform.position - source.Value).normalized * force, ForceMode.Impulse);
        }
        else
        {
            foreach (var rigidbody in rigidbodies)
                rigidbody.AddForce((-rigidbody.transform.forward).normalized * force, ForceMode.Impulse);
        }
    }

    void Switch(bool state)
    {
        foreach (var collider in colliders)
            collider.enabled = state;
        foreach (var rigidbody in rigidbodies)
            rigidbody.isKinematic = !state;
    }
}
