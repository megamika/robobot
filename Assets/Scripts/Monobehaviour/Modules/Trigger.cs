﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Trigger : MonoBehaviour
{
    public Action<Collider> enter;
    public Action<Collider> exit;
    public Action<Collider> stay;

    private void OnTriggerEnter(Collider other)
    {
        enter?.Invoke(other);
    }

    private void OnTriggerExit(Collider other)
    {
        exit?.Invoke(other);
    }

    private void OnTriggerStay(Collider other)
    {
        stay?.Invoke(other);
    }
}
