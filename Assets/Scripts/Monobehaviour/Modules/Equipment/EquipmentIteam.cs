﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface EquipmentIteam
{
    Transform transform { get; }
    Player.Equipment equipment { get; }
}
