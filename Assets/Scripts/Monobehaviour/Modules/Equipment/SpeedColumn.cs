﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedColumn : MonoBehaviour
{
    public float buffMultiplyer;
    public float debuffMultiplyer;
    public string BuffedTeam;

    Dictionary<Entity.SpeedIsAffectable, Entity.MultiplyerValues.Value> entities 
        = new Dictionary<Entity.SpeedIsAffectable, Entity.MultiplyerValues.Value>();

    private void OnTriggerEnter(Collider other)
    {
        Entity isEntity = other.GetComponent<Entity>();
        if (isEntity)
        {
            Entity.SpeedIsAffectable speedIsAffectable = isEntity as Entity.SpeedIsAffectable;
            if (speedIsAffectable != null)
            {
                if (isEntity.Team == BuffedTeam)
                    Buff();
                else
                    Debuff();
            }


            void Buff() => Affect(buffMultiplyer);
            void Debuff() => Affect(debuffMultiplyer);
            void Affect(float multiplyer) => entities.Add(speedIsAffectable, speedIsAffectable.speedMultiplyer.AddMultiplyer(multiplyer));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Entity.SpeedIsAffectable speedIsAffectable = other.GetComponent<Entity.SpeedIsAffectable>();

        if (speedIsAffectable != null)
        {
            entities[speedIsAffectable].parent.RemoveMultiplyer(entities[speedIsAffectable]);
            entities.Remove(speedIsAffectable);
        }
    }
}
