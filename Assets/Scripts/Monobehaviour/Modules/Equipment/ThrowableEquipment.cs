﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ThrowableEquipment : MonoBehaviour, EquipmentIteam
{
    public Entity.Equipment equipment => throwable;
    [SerializeField] Throwable throwable;

    [System.Serializable]
    public class Throwable : Entity.Equipment
    {
        [SerializeField] GameObject throwable;
        [SerializeField] Vector3 throwOffset;


        [SerializeField] float throwForce;
        [SerializeField] float cooldown;

        public void AddSceneThrowable()
        {
            PhysicsObject sceneThrowable = Instantiate(throwable).GetComponent<PhysicsObject>();
            Assert.IsNotNull(sceneThrowable);
            Physics.IgnoreCollision(parent.collider, sceneThrowable.collider);
        }

        public override void OnPress()
        {
            base.OnPress();
            PhysicsObject thrownObject = Instantiate(throwable, parent.transform.position + parent.model.transform.rotation * throwOffset, Quaternion.identity).GetComponent<PhysicsObject>();
            if (thrownObject.collider != null)
            {
                Physics.IgnoreCollision(parent.collider, thrownObject.collider);
            }
            thrownObject.rigidbody.AddForce(parent.model.transform.forward * throwForce, ForceMode.VelocityChange);
             
        }
    }
}
