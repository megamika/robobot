﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AttackEventCatcher : MonoBehaviour
{
    public Action start;
    public Action stop;

    public void AttackStart()
    {
        start?.Invoke();
    }

    public void AttackEnd()
    {
        stop?.Invoke();
    }
}
