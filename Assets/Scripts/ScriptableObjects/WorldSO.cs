﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New World", menuName = "Engine/world")]
public class WorldSO : ScriptableObject
{
    public WorldSettings worldSettings;
}

[System.Serializable]
public class WorldSettings
{
    public LayerMask groundLayer;
}